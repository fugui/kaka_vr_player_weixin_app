var testData = {
	storeId:'00001',
	storeName:'店铺1',
	equipment:'设备1',
	residual:'300.00',
	records:[
		{
			id:'record000',
			type:'0001',
			typeName:'消费',
			date:'2017-01-01 09:09:09',
			sum:800,
		},
		{
			id:'record001',
			type:'0002',
			typeName:'充值',
			date:'2017-01-01 09:09:09',
			sum:200,
		},
		{
			id:'record002',
			type:'0001',
			typeName:'消费',
			date:'2017-01-01 09:09:09',
			sum:600,
		},
	]
}

function person (testData){	
		$("#result").setTemplateElement("template");
		$("#result").processTemplate(testData);
		
}

person (testData);